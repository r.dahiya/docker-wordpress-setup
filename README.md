# Setup wordpress for development using Docker

## Step1: Install docker and docker compose 
https://docs.docker.com/install/

## Step 2: Create a docker-compose.yml file and add service details.
```
version: '2'

services:
  db:
    image: mysql:5.7
    restart: always
    ports:
      - 3306:3306
    environment:
      MYSQL_ROOT_PASSWORD: root
      MYSQL_DATABASE: wordpress
      MYSQL_USER: wordpress
      MYSQL_PASSWORD: wordpress

  wordpress:
    depends_on:
      - db
    image: wordpress:latest
    ports:
      - 8001:80
    restart: always
    environment:
      WORDPRESS_DB_HOST: db:3306
      WORDPRESS_DB_USER: wordpress
      WORDPRESS_DB_PASSWORD: wordpress
  
  phpmyadmin:
    image: phpmyadmin/phpmyadmin
    links:
      - db
    ports:
      - 8083:80
    environment:
      - PMA_USER=root
      - PMA_PASSWORD=root
```
## Step 3: run the project

```
docker-compose up
```

